/******/ (function(modules) { // webpackBootstrap
/******/ 	// install a JSONP callback for chunk loading
/******/ 	function webpackJsonpCallback(data) {
/******/ 		var chunkIds = data[0];
/******/ 		var moreModules = data[1];
/******/ 		var executeModules = data[2];
/******/
/******/ 		// add "moreModules" to the modules object,
/******/ 		// then flag all "chunkIds" as loaded and fire callback
/******/ 		var moduleId, chunkId, i = 0, resolves = [];
/******/ 		for(;i < chunkIds.length; i++) {
/******/ 			chunkId = chunkIds[i];
/******/ 			if(Object.prototype.hasOwnProperty.call(installedChunks, chunkId) && installedChunks[chunkId]) {
/******/ 				resolves.push(installedChunks[chunkId][0]);
/******/ 			}
/******/ 			installedChunks[chunkId] = 0;
/******/ 		}
/******/ 		for(moduleId in moreModules) {
/******/ 			if(Object.prototype.hasOwnProperty.call(moreModules, moduleId)) {
/******/ 				modules[moduleId] = moreModules[moduleId];
/******/ 			}
/******/ 		}
/******/ 		if(parentJsonpFunction) parentJsonpFunction(data);
/******/
/******/ 		while(resolves.length) {
/******/ 			resolves.shift()();
/******/ 		}
/******/
/******/ 		// add entry modules from loaded chunk to deferred list
/******/ 		deferredModules.push.apply(deferredModules, executeModules || []);
/******/
/******/ 		// run deferred modules when all chunks ready
/******/ 		return checkDeferredModules();
/******/ 	};
/******/ 	function checkDeferredModules() {
/******/ 		var result;
/******/ 		for(var i = 0; i < deferredModules.length; i++) {
/******/ 			var deferredModule = deferredModules[i];
/******/ 			var fulfilled = true;
/******/ 			for(var j = 1; j < deferredModule.length; j++) {
/******/ 				var depId = deferredModule[j];
/******/ 				if(installedChunks[depId] !== 0) fulfilled = false;
/******/ 			}
/******/ 			if(fulfilled) {
/******/ 				deferredModules.splice(i--, 1);
/******/ 				result = __webpack_require__(__webpack_require__.s = deferredModule[0]);
/******/ 			}
/******/ 		}
/******/
/******/ 		return result;
/******/ 	}
/******/
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// object to store loaded and loading chunks
/******/ 	// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 	// Promise = chunk loading, 0 = chunk loaded
/******/ 	var installedChunks = {
/******/ 		"main": 0
/******/ 	};
/******/
/******/ 	var deferredModules = [];
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	var jsonpArray = window["webpackJsonp"] = window["webpackJsonp"] || [];
/******/ 	var oldJsonpFunction = jsonpArray.push.bind(jsonpArray);
/******/ 	jsonpArray.push = webpackJsonpCallback;
/******/ 	jsonpArray = jsonpArray.slice();
/******/ 	for(var i = 0; i < jsonpArray.length; i++) webpackJsonpCallback(jsonpArray[i]);
/******/ 	var parentJsonpFunction = oldJsonpFunction;
/******/
/******/
/******/ 	// add entry module to deferred list
/******/ 	deferredModules.push(["./app/assets/scripts/app.js","vendors~main"]);
/******/ 	// run deferred modules when ready
/******/ 	return checkDeferredModules();
/******/ })
/************************************************************************/
/******/ ({

/***/ "./app/assets/scripts/app.js":
/*!***********************************!*\
  !*** ./app/assets/scripts/app.js ***!
  \***********************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! jquery */ \"./node_modules/jquery/dist/jquery.js\");\n/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _styles_style_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../styles/style.css */ \"./app/assets/styles/style.css\");\n/* harmony import */ var _styles_style_css__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_styles_style_css__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var _modules_MobileMenu__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./modules/MobileMenu */ \"./app/assets/scripts/modules/MobileMenu.js\");\n/* harmony import */ var _modules_RevealOnScroll__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./modules/RevealOnScroll */ \"./app/assets/scripts/modules/RevealOnScroll.js\");\n/* harmony import */ var _modules_SmoothScroll__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./modules/SmoothScroll */ \"./app/assets/scripts/modules/SmoothScroll.js\");\n/* harmony import */ var _modules_ActiveLinks__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./modules/ActiveLinks */ \"./app/assets/scripts/modules/ActiveLinks.js\");\n/* harmony import */ var _modules_Modal__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./modules/Modal */ \"./app/assets/scripts/modules/Modal.js\");\n/* harmony import */ var lazysizes__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! lazysizes */ \"./node_modules/lazysizes/lazysizes.js\");\n/* harmony import */ var lazysizes__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(lazysizes__WEBPACK_IMPORTED_MODULE_7__);\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n// alert(\"Hello worldd\");\r\nif(false){}\r\nconsole.log(\"app.js is ready\");\r\n\r\nlet mobileMenu = new _modules_MobileMenu__WEBPACK_IMPORTED_MODULE_2__[\"default\"]();\r\nnew _modules_RevealOnScroll__WEBPACK_IMPORTED_MODULE_3__[\"default\"](jquery__WEBPACK_IMPORTED_MODULE_0___default()(\".section\"),\"40%\");\r\nnew _modules_SmoothScroll__WEBPACK_IMPORTED_MODULE_4__[\"default\"]();\r\nnew _modules_ActiveLinks__WEBPACK_IMPORTED_MODULE_5__[\"default\"]();\r\nnew _modules_Modal__WEBPACK_IMPORTED_MODULE_6__[\"default\"];\n\n//# sourceURL=webpack:///./app/assets/scripts/app.js?");

/***/ }),

/***/ "./app/assets/scripts/modules/ActiveLinks.js":
/*!***************************************************!*\
  !*** ./app/assets/scripts/modules/ActiveLinks.js ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! jquery */ \"./node_modules/jquery/dist/jquery.js\");\n/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _node_modules_waypoints_lib_noframework_waypoints__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../node_modules/waypoints/lib/noframework.waypoints */ \"./node_modules/waypoints/lib/noframework.waypoints.js\");\n/* harmony import */ var _node_modules_waypoints_lib_noframework_waypoints__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_node_modules_waypoints_lib_noframework_waypoints__WEBPACK_IMPORTED_MODULE_1__);\n\r\n\r\nclass ActiveLinks{\r\n    constructor(){\r\n        this.pageSections = jquery__WEBPACK_IMPORTED_MODULE_0___default()(\".section\");\r\n        this.headerLinks = jquery__WEBPACK_IMPORTED_MODULE_0___default()(\".menu-list a\");\r\n        this.createWaypoints();\r\n    }\r\n    createWaypoints(){\r\n        var that = this;\r\n        this.pageSections.each(function(){\r\n            var currentPageSection = this;\r\n            new Waypoint({\r\n                element: currentPageSection,\r\n                handler: function(direction){\r\n                    if(direction == \"down\"){\r\n                        var linkElement = currentPageSection.getAttribute('data-active-link');\r\n                        that.headerLinks.removeClass('active');\r\n                        jquery__WEBPACK_IMPORTED_MODULE_0___default()(linkElement).addClass(\"active\");\r\n                    }\r\n                },\r\n                offset:\"20%\"\r\n            });\r\n            new Waypoint({\r\n                element: currentPageSection,\r\n                handler: function(direction){\r\n                    if(direction == \"up\"){\r\n                        var linkElement = currentPageSection.getAttribute('data-active-link');\r\n                        that.headerLinks.removeClass('active');\r\n                        jquery__WEBPACK_IMPORTED_MODULE_0___default()(linkElement).addClass(\"active\");\r\n                    }\r\n                },\r\n                offset:\"-60%\"\r\n            });\r\n        });\r\n    }\r\n}\r\n\r\n/* harmony default export */ __webpack_exports__[\"default\"] = (ActiveLinks);\n\n//# sourceURL=webpack:///./app/assets/scripts/modules/ActiveLinks.js?");

/***/ }),

/***/ "./app/assets/scripts/modules/MobileMenu.js":
/*!**************************************************!*\
  !*** ./app/assets/scripts/modules/MobileMenu.js ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n// 1. select the element\r\n//2. add listenner to it\r\n//3. write a funct which performs job when event occurs\r\n\r\n\r\nclass MobileMenu{\r\n    constructor(){\r\n        //select the element\r\n        this.menuIcon = document.querySelector(\".mobile-header-icon\");\r\n        this.mobileHeader = document.querySelector(\".mobile-menu\");\r\n        this.events();\r\n    }\r\n    events(){\r\n        this.menuIcon.addEventListener(\"click\",()=>this.toggleMenu());\r\n    }\r\n    toggleMenu(){\r\n        this.mobileHeader.classList.toggle(\"mobile-menu-active\");\r\n        this.menuIcon.classList.toggle(\"mobile-header-icon-close\");\r\n    }\r\n}\r\n/* harmony default export */ __webpack_exports__[\"default\"] = (MobileMenu);\n\n//# sourceURL=webpack:///./app/assets/scripts/modules/MobileMenu.js?");

/***/ }),

/***/ "./app/assets/scripts/modules/Modal.js":
/*!*********************************************!*\
  !*** ./app/assets/scripts/modules/Modal.js ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! jquery */ \"./node_modules/jquery/dist/jquery.js\");\n/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_0__);\n\r\nclass Modal {\r\n    constructor(){\r\n        this.openModalButtons = jquery__WEBPACK_IMPORTED_MODULE_0___default()(\".open-modal\");\r\n        this.modal = jquery__WEBPACK_IMPORTED_MODULE_0___default()(\".modal\");\r\n        this.modalCloseButton = jquery__WEBPACK_IMPORTED_MODULE_0___default()(\".modal-close\");\r\n        this.events();\r\n    }\r\n    events() {\r\n        this.openModalButtons.click(this.openModal.bind(this));\r\n        this.modalCloseButton.click(this.closeModal.bind(this));\r\n        jquery__WEBPACK_IMPORTED_MODULE_0___default()(document).keyup(this.keyPressHandler.bind(this));\r\n    }\r\n    openModal(){\r\n        this.modal.addClass(\"modal-is-visible\");\r\n        return false;\r\n    }\r\n    closeModal(){\r\n        this.modal.removeClass(\"modal-is-visible\");\r\n    }\r\n    keyPressHandler(e) {\r\n        if(e.keyCode == 27) {\r\n            this.closeModal();\r\n        }\r\n    }\r\n}\r\n\r\n/* harmony default export */ __webpack_exports__[\"default\"] = (Modal);\n\n//# sourceURL=webpack:///./app/assets/scripts/modules/Modal.js?");

/***/ }),

/***/ "./app/assets/scripts/modules/RevealOnScroll.js":
/*!******************************************************!*\
  !*** ./app/assets/scripts/modules/RevealOnScroll.js ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! jquery */ \"./node_modules/jquery/dist/jquery.js\");\n/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _node_modules_waypoints_lib_noframework_waypoints__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../node_modules/waypoints/lib/noframework.waypoints */ \"./node_modules/waypoints/lib/noframework.waypoints.js\");\n/* harmony import */ var _node_modules_waypoints_lib_noframework_waypoints__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_node_modules_waypoints_lib_noframework_waypoints__WEBPACK_IMPORTED_MODULE_1__);\n\r\n\r\nclass RevealOnScroll{\r\n    constructor(els,offsetPercentage=\"60%\"){\r\n        this.itemsToReveal = els;\r\n        this.offsetPercentage = offsetPercentage;\r\n        this.hideInitially();\r\n        this.createWaypoints();\r\n    }\r\n    hideInitially(){\r\n        this.itemsToReveal.addClass(\"reveal-item\");\r\n    }\r\n    createWaypoints(){\r\n        var that = this;\r\n        this.itemsToReveal.each(function(){\r\n            var currentElement = this;\r\n            new Waypoint({\r\n                element: currentElement,\r\n                handler: function(){\r\n                    jquery__WEBPACK_IMPORTED_MODULE_0___default()(currentElement).addClass(\"reveal-item-is-visible\");\r\n                },\r\n                offset: that.offsetPercentage\r\n            });\r\n        })\r\n    }\r\n}\r\n\r\n/* harmony default export */ __webpack_exports__[\"default\"] = (RevealOnScroll);\n\n//# sourceURL=webpack:///./app/assets/scripts/modules/RevealOnScroll.js?");

/***/ }),

/***/ "./app/assets/scripts/modules/SmoothScroll.js":
/*!****************************************************!*\
  !*** ./app/assets/scripts/modules/SmoothScroll.js ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! jquery */ \"./node_modules/jquery/dist/jquery.js\");\n/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var jquery_smooth_scroll__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! jquery-smooth-scroll */ \"./node_modules/jquery-smooth-scroll/jquery.smooth-scroll.js\");\n/* harmony import */ var jquery_smooth_scroll__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(jquery_smooth_scroll__WEBPACK_IMPORTED_MODULE_1__);\n\r\n\r\n\r\nclass SmoothScroll{\r\n    constructor(){\r\n        this.headerLinks = jquery__WEBPACK_IMPORTED_MODULE_0___default()(\".smooth-scroll a\");\r\n        this.addSmoothScrolling();\r\n    }\r\n    addSmoothScrolling(){\r\n        this.headerLinks.smoothScroll();\r\n    }\r\n}\r\n\r\n/* harmony default export */ __webpack_exports__[\"default\"] = (SmoothScroll);\n\n//# sourceURL=webpack:///./app/assets/scripts/modules/SmoothScroll.js?");

/***/ }),

/***/ "./app/assets/styles/style.css":
/*!*************************************!*\
  !*** ./app/assets/styles/style.css ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// extracted by mini-css-extract-plugin\n\n//# sourceURL=webpack:///./app/assets/styles/style.css?");

/***/ })

/******/ });