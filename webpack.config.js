const currentTask = process.env.npm_lifecycle_event; // Gives the name of the current task (dev/build)
const path = require("path");
const {CleanWebpackPlugin} = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const fse = require('fs-extra');
const postCSSPlugins = [
    require('postcss-import'),
    require('postcss-mixins'),
    require('postcss-simple-vars'),
    require('postcss-nested'),
    require('postcss-hexrgba'),
    require('autoprefixer')
];

class TaskAfterCompilation{
    apply(compiler){
        //webpack.js.org/api/compiler-hooks
        //used to loada images
        //done is used bcoz we want to use it after compilation
        compiler.hooks.done.tap('Copy Images...',function(){
            fse.copySync('./app/assets/images','./dist/assets/images');
        });
    }
}
let cssConfig ={
    test: /\.css$/i,
    use:[
        'css-loader?url=false',
        {
            loader: 'postcss-loader',
            options:{
                plugins: postCSSPlugins
           }
        }
    ],
};
let config = {
    entry:'./app/assets/scripts/app.js',
    plugins:[
        //used for html file
        new HtmlWebpackPlugin({
            filename: "index.html",
            template: "./app/index.html"
        })
    ],
    module:{
        rules: [
            cssConfig
        ]
    }    
}
if(currentTask == "dev"){
    //this will work when weuse npm run dev, i.e. development purpose
    config.output={
        filename:"app.bundled.js",
        path:path.resolve(__dirname,"app")
    };
    config.devServer={
        before: function(app, server)
        {
            server._watch('./app/**/*.html') 
        },
        contentBase: path.join(__dirname, "app"),
        hot: true,
        port: 3000,
        host: '0.0.0.0'
    };
    config.mode="development";
    cssConfig.use.unshift('style-loader');
}

if(currentTask == "build"){
    //this will be used to generate the build. which will make the website deployable
    config.output={
        //chunkhash will generate a random hash code. it will be updated everytime we change. if not changed it will not reload the file
        filename:"[name].[chunkhash].js",
        chunkFilename: "[name].[chunkhash].js",
        path:path.resolve(__dirname,"dist")
    };
    config.mode="development";
    config.optimization = {
        splitChunks: {
            chunks: "all"
        }
    };
    config.plugins.push(
        //clean webpack plugin is used so that the older files are first removed and then new file will be added else it ll store all files
        new CleanWebpackPlugin(),
        //mini css is used ot generate .min.css which will compresss the css
        new MiniCssExtractPlugin({
            filename: 'styles.[chunkhash].css'
        }),
        new TaskAfterCompilation()
    );
    cssConfig.use.unshift(MiniCssExtractPlugin.loader);
    postCSSPlugins.push(require('cssnano'));
}
module.exports = config;
